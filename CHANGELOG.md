## [2.1.3] - 2022-11-30
## Fixed
- string to float conversion

## [2.1.2] - 2020-08-10
## Fixed
- class namespace

## [2.1.1] - 2020-07-28
## Fixed
- composer

## [2.1.0] - 2020-07-28
## Fixed
- tax class
- qty type

## [2.0.2] - 2020-07-14
## Fixed
- tax class

## [2.0.1] - 2020-06-07
## Fixed
- fatal error

## [2.0.0] - 2020-05-20
## Fixed
- refactor 

## [1.0.2] - 2020-10-06
## Fixed
- gross price

## [1.0.1] - 2020-10-04
## Fixed
- price rounding

## [1.0.0] - 2020-09-16
### Added
- init
